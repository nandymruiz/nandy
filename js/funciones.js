$(function(){
    let tablero=[["","",""],["","",""],["","",""]];
    let jugarActual='X';
    $("#mensaje").hide();
    $('td').click(function(){
        let celda = $(this);//OBTIENE LA CELDA
        //Obtiene la fila y la columna
        let fila=celda.attr('fila');
        let col=celda.attr('col');
        //obtiene el valor en el tablero
        let valorTablero=tablero[fila][col]
        //Muestra la jugada en la página
        celda.text(jugarActual);
        //Valida si la celda esta vacia
        if(valorTablero===''){
            $("#mensaje").hide();
            //Guarda la jugada en el tablero
            tablero[fila][col]=jugarActual;
            //muestra la jugada en la página
            celda.text(jugarActual);
            celda.addClass("celdaJugada");
            $("#eventos").append("<li class='list-group-item list-group-item-info'>"+jugarActual+" hizo una jugada</li>");
            //Verifica si hay triqui
            if (
                //Horizontal 1 ddd
                (tablero [0][0] !== '' && tablero[0][0] === tablero[0][1] && tablero[0][1] === tablero[0][2]) ||
                //Horizontal 2 ff
                (tablero [1][0] !== '' && tablero[1][0] === tablero[1][1] && tablero[1][1] === tablero[1][2]) ||
                //Horizontal 3
                (tablero [2][0] !== '' && tablero[2][0] === tablero[2][1] && tablero[2][1] === tablero[2][2]) ||
                //Vertical 1
                (tablero [0][0] !== '' && tablero[0][0] === tablero[1][0] && tablero[1][0] === tablero[2][0]) ||
                //Vertical 2
                (tablero [0][1] !== '' && tablero[0][1] === tablero[1][1] && tablero[1][1] === tablero[2][1]) ||
                //Vertical 3
                (tablero [0][2] !== '' && tablero[0][2] === tablero[1][2] && tablero[1][2] === tablero[2][2]) ||
                //diagonal 1
                (tablero [0][0] !== '' && tablero[0][0] === tablero[1][1] && tablero[1][1] === tablero[2][2]) ||
                //diagonal2
                (tablero [0][2] !== '' && tablero[0][2] === tablero[1][1] && tablero[1][1] === tablero[0][1]) 
            ){
                $("#eventos").append('<li class="list-group-item list-group-item-success">'+jugarActual+" Hizo Triqui!!!</li>");

                $("#mensaje").text("Has ganado! jugador "+jugarActual);
            }
            //Cambio de Jugador
            if (jugarActual==='X') {
                jugarActual='O';
            } else {
                jugarActual='X';
            }
            $("#textoInformacion").text("Turno de " + jugarActual);
        }
        console.table(tablero);
       // console.log(fila,col);
        
    });

});